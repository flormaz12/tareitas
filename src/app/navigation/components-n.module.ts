import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavtabsComponent } from './navtabs/navtabs.component';
import { SidenavListComponent } from './sidenav-list/sidenav-list.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
@NgModule({
  declarations: [
    NavtabsComponent,
    SidenavListComponent,
    ToolbarComponent
  ],
  exports:[
    NavtabsComponent,
    SidenavListComponent,
    ToolbarComponent
  ],
  imports: [
    CommonModule,
    
  ]
})
export class ComponentsNModule { }
