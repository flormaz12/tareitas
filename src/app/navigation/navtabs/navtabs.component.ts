import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-navtabs',
  templateUrl: './navtabs.component.html',
  styleUrls: ['./navtabs.component.css']
})
export class NavtabsComponent implements OnInit {
@Input() navtabs: any;
  constructor() { }

  ngOnInit(): void {
  }

}
